const mongoose = require("../data");

let artistaSchema = new mongoose.Schema(
  {
    nome: String,
    pais: String,
    iniciaAtividade: Number,
    fimAtividade: Number,
  },
  { timestamps: true }
);

module.exports = mongoose.model("artista", artistaSchema);
