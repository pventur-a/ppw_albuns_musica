const mongoose = require('../data')
const artista = require('../models/Artista')

let albumSchema = new mongoose.Schema({
  nome: String,
  artista: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'artista'
  },
  ano: Number,
  genero: Array,
  faixas: Array,
  lancamento: Number
}, { timestamps: true })

module.exports = mongoose.model('Album', albumSchema)

