const express = require("express");
const app = express();
const PORT = process.env.PORT || 8080;
const routes = require("./routes");
const errorHandler = require("./middleware/errorHandler");

app.use("/api", routes);
app.use(errorHandler);

app.listen(PORT, () => {
  console.log(`Servidor foi iniciado ns porta ${PORT}`);
});
