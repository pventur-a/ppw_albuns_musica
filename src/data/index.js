const mongoose = require("mongoose");
const options = { useNewUrlParser: true, useUnifiedTopology: true };
const URL = require("../../env.json");

mongoose.connect(URL.connection, options, function (err) {
  if (!err) {
    console.log("Conectado ao banco");
  }
});

module.exports = mongoose;
