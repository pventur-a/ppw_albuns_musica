const express = require("express");
const router = express.Router();
const Artista = require("../models/Artista");

router.get("/", async (req, res, next) => {
  try {
    const doc = await Artista.find();
    res.json(doc);
  } catch (error) {
    next(error);
  }
});
router.get("/:id", async (req, res, next) => {
  try {
    const id = req.params.id;
    const doc = await Artista.findById(id);
    res.json(doc);
  } catch (error) {
    next(error);
  }
});

router.post("/", async (req, res, next) => {
  try {
    const body = req.body;
    const doc = new Artista(body);
    await doc.save();
    res.json(doc);
  } catch (error) {
    next(error);
  }
});
router.put("/:id", async (req, res, next) => {
  try {
    const body = req.body;
    const id = req.params.id;
    const doc = await Artista.findByIdAndUpdate(id, body);
    res.json(doc);
  } catch (error) {
    next(error);
  }
});

router.delete("/:id", async (req, res, next) => {
  try {
    const id = req.params.id;
    const doc = await Artista.findByIdAndDelete(id);
    res.json(doc);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
