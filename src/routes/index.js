const express = require("express");
const router = express.Router();
const routes = {
  albuns: require("./albuns"),
  artistas: require("./artistas"),
};

// Middleware que converte o body para um objeto
router.use(express.json());
//router.use(express.urlencoded())

router.use("/albuns", routes.albuns);
router.use("/artistas", routes.artistas);
router.get("/", (req, res, next) => {
  res.json({
    name: "Pedro Ventura",
    endpoint: ["api/albuns", "api/artistas"],
  });
});

module.exports = router;
